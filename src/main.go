package main

import (
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strings"
)

var (
	regexSRC    *regexp.Regexp = regexp.MustCompile("src=\"([^\"]+)\"")
	regexHREF   *regexp.Regexp = regexp.MustCompile("href=\"([^\"]+)\"")
	regexImage  *regexp.Regexp = regexp.MustCompile("<(img|IMG)[^>]+>")
	regexAnchor *regexp.Regexp = regexp.MustCompile("<[aA][^>]+>")
	statii      map[string]int = make(map[string]int)
	seed        string
	seedURL     *url.URL
)

func init() {
	flag.StringVar(&seed, "seed", "", "Crawler seed in the form of a URL.")
}

func main() {
	flag.Parse()

	if seed == "" {
		flag.PrintDefaults()
	}

	var err error
	seedURL, err = url.Parse(seed)
	if err != nil {
		log.Fatal(err)
	}

	seedURL.Fragment = ""
	seedURL.RawQuery = ""
	statii[seedURL.String()] = 0

	var progress int = 0

	next := nextURL()
	for next != nil {
		progress++
		log.Printf("%d/%d (%d%%): %s", progress, len(statii), progress*100/len(statii), next.String())
		status, hrefs := getHREFs(next)
		statii[next.String()] = status

		if status == 200 {
			for _, href := range hrefs {
				u, err := url.Parse(href)
				if err != nil {
					log.Println(err)
					continue
				}
				queueURL(u)
			}
		}

		next = nextURL()
	}

	fmt.Println("URLs Scanned:")
	for href, status := range statii {
		if status > 0 {
			fmt.Printf("%d: %s\n", status, href)
		}
	}

	fmt.Println("Unscanned URLs:")
	for href, status := range statii {
		if status < 0 {
			fmt.Printf("%s\n", href)
		}
	}

	fmt.Println("Failed URLs:")
	for href, status := range statii {
		if (status != 200) && (status > 0) {
			fmt.Printf("%d: %s\n", status, href)
		}
	}
}

func skipURL(u *url.URL) {

	// Clean it up.
	u.Fragment = ""
	u.RawQuery = ""
	u = seedURL.ResolveReference(u)
	if u.Host != seedURL.Host {
		return
	}

	// Add it if it isn't in the queue already.
	_, ok := statii[u.String()]
	if !ok {
		statii[u.String()] = -1
	}
}

func queueURL(u *url.URL) {

	// Clean it up.
	u.Fragment = ""
	u.RawQuery = ""
	u = seedURL.ResolveReference(u)

	status := 0
	if u.Host != seedURL.Host {
		status = -1
	}

	// Add it if it isn't in the queue already.
	_, ok := statii[u.String()]
	if !ok {
		statii[u.String()] = status
	}
}

func nextURL() *url.URL {
	for href, status := range statii {
		if status == 0 {
			u, err := url.Parse(href)
			if err != nil {
				log.Fatal(err)
			}
			return u
		}
	}
	return nil
}

func getHREFs(u *url.URL) (status int, hrefs []string) {

	var (
		resp *http.Response
		err  error = errors.New("")
	)

	for err != nil {
		resp, err = http.Get(u.String())
		if err != nil {
			log.Println(err)
		}
	}

	status = resp.StatusCode

	if status != 200 {
		return
	}

	var isHTML bool = false
	for _, value := range resp.Header["Content-Type"] {
		if strings.Contains(value, "text/html") {
			isHTML = true
		}
	}

	if !isHTML {
		return
	}

	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		log.Fatal(err)
	}

	anchors := extractAnchors(body)
	for _, anchor := range anchors {
		href := extractHREF(anchor)
		if href != "" {
			hrefs = append(hrefs, href)
		}
	}

	images := extractImages(body)
	for _, image := range images {
		src := extractSRC(image)
		if src != "" {
			hrefs = append(hrefs, src)
		}
	}

	return
}

func extractHREFs(body []byte) []string {
	var hrefs []string

	matches := regexHREF.FindAllSubmatch(body, -1)

	for _, match := range matches {
		if len(match) == 2 {
			href := fmt.Sprintf("%s", match[1])
			hrefs = append(hrefs, href)
		}
	}

	return hrefs
}

func extractAnchors(body []byte) (anchors [][]byte) {

	anchors = regexAnchor.FindAll(body, -1)
	return
}

func extractImages(body []byte) (images [][]byte) {
	images = regexImage.FindAll(body, -1)
	return
}

func extractHREF(anchor []byte) (href string) {
	matches := regexHREF.FindSubmatch(anchor)

	if len(matches) == 2 {
		href = fmt.Sprintf("%s", matches[1])
	}

	return
}

func extractSRC(image []byte) (src string) {
	matches := regexSRC.FindSubmatch(image)

	if len(matches) == 2 {
		src = fmt.Sprintf("%s", matches[1])
	}
	return
}
